﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NLog;

namespace LogSender
{
    class Program
    {
        private static Logger _logger = LogManager.GetLogger("Program");

        static void Main(string[] args)
        {
            while (true)
            {
                Thread.Sleep(5000);
                _logger.Info(" Hello time is : " + DateTime.Now.ToString());
            }
        }
    }

    // NLog.LogReceiverService.ILogReceiverClient
}
